# Docker CE Swarm in Vagrant

## Intoduction

## How to Use
1. Clone the Repository
2. Edit configuration files if neccesary see [provisioning/group_vars/all/auth.yml](provisioning/group_vars/all/auth.yml)
3. Amend number of MANAGER_COUNT and/or WORKER_COUNT is neccesary [Vagrantfile](Vagrantfile)
4. Issue vagrant up from terminal

## Author
Aaron Guise [Say hi!](mailto:aaron@guise.net.nz)
