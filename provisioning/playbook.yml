---
- name: Install Docker
  hosts: all
  become: true
  handlers:
    - name: Start Docker
      service:
        name: docker
        state: restarted
        enabled: true
    - name: Start GlusterFS
      service:
        name: glusterd
        state: restarted
        enabled: true
  tasks:
    - name: Install pre-requisite packages
      yum:
        name: "yum-utils,device-mapper-persistent-data, lvm2"
        state: installed

    - name: Add Docker CE Repository
      command: yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

    - name: Install Docker
      yum:
        name: "docker-ce,docker-ce-cli,containerd.io"
        state: installed
      notify:
        - Start Docker
    
    - name: Install Storage SIG
      yum:
        name: centos-release-gluster
        state: installed
      
    - name: Install GlusterFS-Server
      yum:
        name: glusterfs-server
        state: installed
      notify: Start GlusterFS

- name: Configure Gluster Replication
  hosts: all
  become: true
  tasks:
    - name: Ensure Gluster Volume Parent Directory exists
      file:
        path: /var/lib/gluster_volumes
        state: directory

    - name: Ensure /data directory exists
      file:
        path: /data
        state: directory
    
    - name: Ensure /data/swarm-vols directory exists
      file:
        path: /data/swarm-vols
        state: directory
    
    # - name: Wait allow time for gluster to start
    #   pause:
    #     seconds: 30
    #   run_once: true

    - name: ping each peer
      shell: gluster peer probe {{ item }}
      with_items: "{{ ansible_play_hosts }}"
      run_once: true

    - name: create swarm volume
      shell: "{{ lookup('template', './templates/gluster-create.j2') }}"
      retries: 5
      delay: 20
      run_once: true

  
    - name: Wait allow time for gluster to start
      pause:
        seconds: 30
      run_once: true

    - name: Allow Localhost Authentication
      shell: "gluster volume set swarm-vols auth.allow 127.0.0.1"
      retries: 5
      delay: 20
      run_once: true

    - name: Start swarm-vol replication
      shell: gluster volume start swarm-vols
      retries: 3
      delay: 20
      run_once: true

    - name: Mount Gluster FileSystem (All Nodes)
      shell: mount.glusterfs localhost:/swarm-vols /data/swarm-vols
      retries: 3
      delay: 20

    - name: Touch Files
      file:
        path: /data/swarm-vols/{{ ansible_hostname }}.txt
        state: touch

    - name: Ensure we have our GlusterFS Volume mount at boot
      mount:
        path: /data/swarm-vols
        src: localhost:/swarm-vols
        fstype: glusterfs
        opts: defaults,_netdev,noauto,x-systemd.automount
        state: present

- hosts: managers
  become: true
  roles: []
  tasks: 
    - name: Check Status of docker swarm
      shell: docker info
      register: docker_info
      changed_when: false

    - block:
        - name: Get Manager Join Token 
          shell: docker swarm join-token -q manager
          register: manager_token_a
        - name: Get Worker Join Token
          shell: docker swarm join-token -q worker
          register: worker_token_a
      when: "ansible_hostname == 'manager1' and docker_info.stdout.find('Swarm: active') != -1"

    - block:
        - name: Initialise Swarm
          shell: docker swarm init --advertise-addr {{ ansible_eth1['ipv4']['address'] }}
        - name: Get Manager Join Token 
          shell: docker swarm join-token -q manager
          register: manager_token_b
        - name: Get Worker Join Token
          shell: docker swarm join-token -q worker
          register: worker_token_b
      when: "inventory_hostname == 'manager1' and docker_info.stdout.find('Swarm: inactive') != -1"

    - name: Persist facts subsequent runs
      set_fact:
        worker_token: "{{ worker_token_a.stdout }}"
        manager_token: "{{ manager_token_a.stdout }}"
      when: "inventory_hostname == 'manager1' and (worker_token_a.stdout is defined and worker_token_a.skipped is not defined)"

    - name: Persist facts Initial Run
      set_fact:
        worker_token: "{{ worker_token_b.stdout }}"
        manager_token: "{{ manager_token_b.stdout }}"
      when: "inventory_hostname == 'manager1' and (worker_token_b.stdout is defined and worker_token_b.skipped is not defined)"

    - block:
        - name: Join as Manager
          shell: "docker swarm join --token {{ hostvars['manager1']['manager_token'] }} {{ hostvars['manager1']['ansible_eth1']['ipv4']['address'] }}:2377"
          when: docker_info.stdout.find('Swarm{{':'}} inactive') != -1
          retries: 3
          delay: 20
      when: inventory_hostname != 'manager1'

    - name: Portainer install
      block: 
        - name: Get Docker Secrets
          shell: docker secret ls
          register: docker_secrets

        - name: Create Secret for Portainer
          shell: printf {{ portainer_password }} | docker secret create portainer_pass -
          when: docker_secrets.stdout.find('portainer_pass') == -1
          run_once: true

        - name: Get Portainer Stack info
          shell: docker stack ls
          register: docker_stack
          run_once: true

        - name: Create Data Directory
          file:
            path: /data/swarm-vols/portainer_data
            state: directory

        - name: Install Stack
          shell: docker stack deploy -c portainer-agent-stack.yml portainer
          args:
            chdir: /vagrant/docker_stacks
          when: docker_stack.stdout.find('portainer') == -1
          run_once: true


      
- hosts: workers
  become: true
  roles: []
  tasks:
    - name: Check Status of docker swarm
      shell: docker info
      register: docker_info
      changed_when: false
    - block:
        - name: Join as worker
          shell: "docker swarm join --token {{ hostvars['manager1']['worker_token'] }} {{ hostvars['manager1']['ansible_eth1']['ipv4']['address'] }}:2377"
          when: >-
            docker_info.stdout.find("Swarm: inactive") != -1
          retries: 3
          delay: 20