# -*- mode: ruby -*-
# vi: set ft=ruby :

# The Amount of Docker Swarm Managers to create
MANAGER_COUNT = 1
# The Amount of Docker Swarm Workers to create
WORKER_COUNT = 2


Vagrant.configure("2") do | config |
  # All Boxes utilize CentOS 7
  config.vm.box = "bento/centos-7"
  config.vbguest.auto_update = false
  config.hostmanager.enabled = true
  config.hostmanager.manage_host = true
  config.hostmanager.manage_guest = true
  config.hostmanager.ignore_private_ip = false
  config.hostmanager.include_offline = false

  (1..WORKER_COUNT).each do |i|
    config.vm.define "worker#{i}" do | worker |
      worker.vm.network "private_network", ip: "192.168.34.#{i + 20}"
      worker.vm.hostname = "worker#{i}.swarmup"
      worker.hostmanager.aliases = ["worker#{i}"]
    end
  end

  config.vm.provider "virtualbox" do | vb |
    # Customize the amount of memory on the VM:
    vb.cpus = "2"
    vb.memory = "1024"
  end

  config.vm.provider "vmware_desktop" do | vmware |
    # Customize the amount of memory on the VM:
    vmware.cpus = "2"
    vmware.memory = "1024"
    vmware.whitelist_verified = true
  end
  
  if MANAGER_COUNT >= 2
    (2..MANAGER_COUNT).each do |i|
      config.vm.define "manager#{i}" do | manager |
        manager.vm.network "private_network", ip: "192.168.34.#{i + 10}"
        manager.vm.hostname = "manager#{i}.swarmup"
        manager.hostmanager.aliases = ["manager#{i}"]
      end
    end
  end

  config.vm.define "manager1" do | manager |
    manager.vm.network "private_network", ip: "192.168.34.11"
    manager.vm.hostname = "manager1.swarmup"
    manager.hostmanager.aliases = %w(manager1)
    manager.vm.provision "shell", inline: <<-SHELL
      # Install sshpass for ansible with password
      sudo yum install -y sshpass
    SHELL
    manager.vm.provision "ansible_local" do | ansible |
      ansible.playbook = "provisioning/playbook.yml"
      #ansible.galaxy_role_file = "provisioning/roles/requirements.yml"
      ansible.install_mode = "pip"
      ansible.version = "2.5.0"
      ansible.groups = {
        "managers" => ["manager[1:#{MANAGER_COUNT}]"],
        "workers" => ["worker[1:#{WORKER_COUNT}]"]
      }
      ansible.limit = "all"
      end
  end

  # config.vm.provision "ansible_local" do | ansible |
  #   ansible.playbook = "provisioning/common.yml"
  #   ansible.galaxy_role_file = "provisioning/roles/requirements.yml"
  #   ansible.install_mode = "pip"
  #   ansible.version = "2.5.0"
  # end
  # Provisions all Virtual Machines as below
  config.vm.provision "shell", inline: <<-SHELL
  #    # Install Docker CE
  #    sudo yum install -y yum-utils \
  #    device-mapper-persistent-data \
  #    lvm2
  #    sudo yum-config-manager \
  #    --add-repo \
  #    https://download.docker.com/linux/centos/docker-ce.repo && \
  #    sudo yum install -y docker-ce docker-ce-cli containerd.io && \
  #    # Enable and Start Docker
  #    sudo systemctl enable docker && \
  #    sudo systemctl restart docker
  SHELL

end
